# require 'byebug'

class Board
  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    grid = [[nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]]
    grid
  end

  def [](position)
    row = position[0]
    column = position[1]
    grid[row][column]
  end

  def []=(position, value)
    row = position[0]
    column = position[1]
    grid[row][column] = value
  end

  def count
    ship_count = 0
    grid.each do |row|
      row.each do |cell|
        if cell == :s
          ship_count += 1
        end
      end
    end
    ship_count
  end

  def empty?(position = nil)
    if count == 0
      true
    elsif position
      self[position].nil?
    end
  end

  def full?
    grid.each do |row|
      row.each do |cell|
        if cell.nil?
          return false
        end
      end
    end
    true
  end

  def place_random_ship
    if full?
      raise "The board is full!"
    end

    position = random_position
    until empty?(position)
      position = random_position
    end
    self[position] = :s
  end

  def random_position
    rand_row = rand(9)
    rand_cell = rand(9)

    [rand_row, rand_cell]
  end

  def display
    grid.each do |row|
      row.each do |cell|
        if cell.nil?
          print " ~ "
        else
          print " #{cell} "
        end
      end
      puts
    end
  end

  def won?
    if count == 0
      true
    end
  end

end
