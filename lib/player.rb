class HumanPlayer
  def initialize(name)
    @name = name
  end

  def get_play
    puts "Please enter a position to attack ( , ): "
    position = gets.chomp
    x = position[1].to_i
    y = position[3].to_i
    [x, y]
  end

end
