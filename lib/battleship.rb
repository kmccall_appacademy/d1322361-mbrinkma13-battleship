require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :player, :board

  def initialize(player = HumanPlayer.new("Mike"), board = Board.new)
    @player = player
    @board = board
  end

  def attack(position)
    board[position] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    position = player.get_play
    attack(position)
    board.display
  end

  def play
    while game_over? == false
      play_turn
    end
  end

end

if __FILE__ == $PROGRAM_NAME
  game = BattleshipGame.new
  game.play
end
